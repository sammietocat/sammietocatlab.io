# YAML Ain't Markup Language

## Table of Contents
#### [Chapter 1. Introduction](#chapter-1-intro)
  * [1.1 Goals](#11-goals)
  * [1.2 Prior Art](#12-prior-art)
  * [1.3 Relation to JSON](#13-relation-to-jason)
  * [1.4 Relation to XML](#14-relation-to-xml)
  * [1.5 Terminology](#15-terminology)

#### [Chapter 2. Preview](#chapter-2-preview)
  * [2.1 Collections](#21-collections)
    + [2.1.1 Flow style](#211-flow-style)
  * [2.2 structures](#22-structures)
  * [2.3 Scalars](#23-scalars)
    + [2.3.1 Flow style](#231-flow-style)
  * [2.4 Tags](#24-tags)
  * [2.5 Full Length Full Length Examples](#25-full-length-examples)

#### [Chapter 3. Processing YAML Information](#chapter-3-processing-yaml-information)
  * [3.1 Processes](#31-Processes)
    + [3.1.1 Dump](#311-dump)
    + [3.1.2 Load](#312-load)
  * [3.2 Information Models](#32-information-models)
    + [3.2.1 Representation Graph](#321-representation-graph)
      - [3.2.1.1 Nodes](#3211-nodes)
      - [3.2.1.2 Tags](#3212-tags)
      - [3.2.1.3 Node Comparison](#3213-node-comparison)
    + [3.2.2 Serialization Tree](#322-serialization-tree)
      - [3.2.2.1 Keys Order](#3221-keys-order)
      - [3.2.2.2 Anchors and Aliases](#3222-anchors-and-aliases)
    + [3.2.3 Presentation Stream](#323-presentation-stream)
      - [3.2.3.1 Node Styles](#3231-node-styles)
      - [3.2.3.2 Scalar Formats](#3232-scalar-formats)
      - [3.2.3.3 Comments](#3233-comments)
      - [3.2.3.4 Directives](#3234-directives)
  * [3.3 Loading Failure Points](#33-loading-failure-points)
    + [3.3.1 Well-Formed Streams and Identified Aliases](#331-well-formed-streams-and-identified-aliases)
    + [3.3.2 Resolved Tags](#332-resolved-tags)
    + [3.3.3 Recognized and Valid Tags](#333-recognized-and-valid-tags)
    + [3.3.4 Available Tags](#334-available-tags)
---

## Chapter 1. Introduction

### Definition
  A data serialization language.

### Components
* 3 primitives  
  + mappings(hashes/dictionaries)
  + sequences(arrays/lists)
  + scalars(strings/numbers)
* A simple typing system
* A aliasing mechanism

### Use cases

* Configuration files
* Log files
* Interprocess messaging
* Cross-language data sharing
* Object persistence
* Debugging of complex data structures

### 1.1 Goals

In decreasing priority  
1. Human-friendly readability 
2. Cross-languange data portability
3. Compatible data structures of agile languages
4. Consistent model to support generic tools
5. One-pass processing support
6. Expressiveness and extensibility
7. Ease of implementation and usability

### 1.2 Prior Art

YAML integrates and builds upon concepts described by `C`, `Java`, `Perl`, `Python`, `Ruby`, `RFC0822 (MAIL)`, `RFC1866 (HTML)`, `RFC2045 (MIME)`, `RFC2396 (URI)`, `XML`, `SAX`, `SOAP`, and `JSON`.

### 1.3 Relation to JSON

JSON’s foremost design goal is simplicity and universality, while YAML’s foremost design goals are human readability and support for serializing arbitrary native data structures.  
YAML can therefore be viewed as a natural superset of JSON, offering improved human readability and a more complete information model.

### 1.4 Relation to XML

No direct correlation exists between them. YAML is primarily a data serialization language. XML was designed to be backwards compatible with the Standard Generalized
Markup Language (SGML), which was designed to support structured documentation.

### 1.5 Terminology

* **May**: The word *may*, or the adjective *optional*, mean that conforming YAML processors are permitted to, but *need not* behave as described.  
* **Should**: The word *should*, or the adjective *recommended*, mean that there could be reasons for a YAML processor to deviate from the behavior described, but that such deviation could hurt interoperability and should therefore be advertised with appropriate notice.  
* **Must**: The word *must*, or the term *required* or *shall*, mean that the behavior described is an absolute requirement of the specification.

## Chapter 2. Preview

### 2.1 Collections

Use indentation for scope and begin each entry on its own line.

* **Sequence**: each entry with a **dash** and **space** (`- `)

```
- Mark McGwire
- Sammy Sosa
- Ken Griffey
```

* **Mapping**: use a **colon** and **space** (`: `) to mark each `key: value` pair

```
hr: 65
avg: 0.278
rbi: 147
```

* **Comment**: begin with `#`

* **Sequence** of **Mapping**

```
american:
  - Boston Red Sox
  - Detroit Tigers
  - New York Yankees
national:
  - New York Mets
  - Chicago Cubs
  - Atlanta Braves
```

* **Mapping** of **Sequence** 

```
-
  name: Mark McGwire
  hr:
  65
  avg: 0.278
-
  name: Sammy Sosa
  hr: 63
  avg: 0.288
```

#### 2.1.1 Flow style

Use explicit indicators rather than indentation to denote scope.

* **sequence of sequences**

```
- [name, hr, avg ]
- [Mark McGwire, 65, 0.278]
- [Sammy Sosa, 63, 0.288]
```

* **mapping of mappings**

```
Mark McGwire: {hr: 65, avg: 0.278}
Sammy Sosa: {
    hr: 63,
    avg: 0.288
  }
```

### 2.2 structures

* `---`

  + separate directives from document content
  + signal the start of a document if no directives are present

```
# Ranking of 1998 home runs
---
- Mark McGwire
- Sammy Sosa
- Ken Griffey

# Team ranking
---
- Chicago Cubs
- St Louis Cardinals
```

* `...`

  + indicate the end of a document without starting a new one

```
---
time: 20:03:20
player: Sammy Sosa
action: strike (miss)
...
---
time: 20:03:47
player: Sammy Sosa
action: grand slam
...
```

* Repeated nodes(objects)

  + first identified by an anchor(`&`)
  + then referenced with an asterisk - `*`

```
---
hr:
  - Mark McGwire
  # Following node labeled SS
  - &SS Sammy Sosa
rbi:
  - *SS # Subsequent occurrence
  - Ken Griffey
```

* `? `

  + indicate a complex mapping key
  + Within a block collection, `key: value` pairs can start immediately following the dash, colon, or question mark

```
? - Detroit Tigers
  - Chicago cubs
:
  - 2001-07-23

? [ New York Yankees,
    Atlanta Braves ]
: [ 2001-07-02, 2001-08-12,
    2001-08-14 ]
```

### 2.3 Scalars

* Literal style: indicated by `|` where all line breaks are significant

```
# ASCII Art
--- |
  \//||\/||
  // || ||__
```

* Folded style: each line break is folded to a space unless it ends an empty or a more-indented line

```
--- >
  Mark McGwire's
  year was crippled
  by a knee injury.
```

#### 2.3.1 Flow style

* double-quoted style: allows escape
* single-quoted style: no escape

```
unicode: "Sosa did fine.\u263A"
control: "\b1998\t1999\t2000\n"
hex esc: "\x0d\x0a is \r\n"

single: '"Howdy!" he cried.'
quoted: ' # Not a ''comment''.'
tie-fighter: '|\-*-/|'
```

```
plain:
  This unquoted scalar
  spans many lines.

quoted: "So does this
  quoted scalar.\n"
```

*Note*:
  * All flow scalars can span multiple lines
  * Line breaks are always folded.

### 2.4 Tags

* Untagged nodes are given a type depending on the application
* Types from fail safe schema
  + `seq`
  + `map`
  + `str`
* Types from JSON schema
  + `int`
  + `float`
  + `null`
* Additional
  + `binary`
  + `omap`
  + `set`

**integer**
```
  canonical: 12345
  decimal: +12345
  octal: 0o14
  hexadecimal: 0xC
```

**float**
```
canonical: 1.23015e+3
exponential: 12.3015e+02
fixed: 1230.15
negative infinity: -.inf
not a number: .NaN
```

**miscellaneous**
```
null:
booleans: [ true, false ]
string: '012345'
```

**timestamps**
```
canonical: 2001-12-15T02:59:43.1Z
iso8601: 2001-12-14t21:59:43.10-05:00
spaced: 2001-12-14 21:59:43.10 -5
date: 2002-12-14
```

Explicit typing is denoted with a tag using the exclamation point (`!`) symbol.

```
---
not-date: !!str 2002-04-28

picture: !!binary |
  R0lGODlhDAAMAIQAAP//9/X
  17unp5WZmZgAAAOfn515eXv
  Pz7Y6OjuDg4J+fn5OTk6enp
  56enmleECcgggoBADs=

application specific tag: !something |
  The semantics of the tag
  above may be different for
  different documents.
```

* Global tags are URIs and may be specified in a
tag shorthand notation using a handle

```
%TAG ! tag:clarkevans.com,2002:
--- !shape
  # Use the ! handle for presenting
  # tag:clarkevans.com,2002:circle
- !circle
  center: &ORIGIN {x: 73, y: 129}
  radius: 7
- !line
  start: *ORIGIN
  finish: { x: 89, y: 102 }
- !label
  start: *ORIGIN
  color: 0xFFEEBB
  text: Pretty vector drawing.
```

* Application-specific local tags may also be used

Unordered Sets
```
# Sets are represented as a
# Mapping where each key is
# associated with a null value
--- !!set
? Mark McGwire
? Sammy Sosa
? Ken Griff
```

Ordered Mappings
```
# Ordered maps are represented as
# A sequence of mappings, with
# each mapping having one key
--- !!omap
  - Mark McGwire: 65
  - Sammy Sosa: 63
  - Ken Griffy: 58
```

### 2.5 Full Length Examples

Invoice

```
--- !<tag:clarkevans.com,2002:invoice>
invoice: 34843
date: 2001-01-23
bill-to: &id001
  given : Chris
  family : Dumars
  address:
    lines: |
        458 Walkman Dr.
        Suite #292
    city    : Royal Oak
    state   : MI
    postal  : 48046
ship-to: *id001
product:
  - sku         : BL394D
    quantity    : 4
    description : Basketball
    price       : 450.00
  - sku         : BL4438H
    quantity    : 1
    description : Super Hoop
    price       : 2392.00
tax : 251.42
total: 4443.52
comments:
  Late afternoon is best.
  Backup contact is Nancy
  Billsmer @ 338-4338.
```

Log File

```
---
Time: 2001-11-23 15:01:42 -5
User: ed
Warning:
  This is an error message
  for the log file
---
Time: 2001-11-23 15:02:31 -5
User: ed
Warning:
  A slightly different error
  message.
---
Date: 2001-11-23 15:03:17 -5
User: ed
Fatal:
  Unknown variable "bar"
Stack:
  - file: TopClass.py
    line: 23
    code: |
      x = MoreObject("345\n")
  - file: MoreClass.py
    line: 58
    code: |-
    foo = bar
```

---
## Chapter 3. Processing YAML Information
**4 Concepts**
* *YAML representation*: a class of data objects
* *YAML stream*: a syntax for presenting YAML representations as a series of characters
* *Processor*: a tool for converting information between above complementary views
* *Application*

**2 Ways to Use YAML Information**
* machine processing
* human consumption

**3 Stages** to reconcile *machine processing* and *human consumption*
* representation: format native data structures to achieve portability
* serialization: turning a YAML representation into a serial form
* presentation: formatting of a YAML serialization for human reading

### 3.1 Processes
Figure 3.1 Processing Overview  
![Processing Overview](./images/Processing-Overview.png)

*Note*: Direct translation between native data
structures and a character stream (*dump* and *load* in the diagram above) is allowed, but with the native data structures constructed only from information available in the representation

#### 3.1.1 Dump
3 stages to dump native data structures to a character stream

1. Representing Native Data Structures   
  * 3 node kinds: `sequence`, `mapping`, `scalar`
  * node = kind + content + tag (specifying data type)  
2. Serializing the Representation Graph  
  * serialize representation to ordered tree for sequential access mediums  
  * impose ordering on the mapping keys and replace second and subsequent references with aliases  
  * serialization details are application-specific 
  * output is a serialization tree  
3. Presenting the Serialization Tree  
  * present the YAML serialization as a character stream in a human-friendly manner
  * can introduce stylistic options

#### 3.1.2 Load
3 stages to load native data structures from a character stream

1. Parsing the Presentation Stream
  * take a stream of characters
  * discard all presentation details
  * report only the serialization events  
2. Composing the Representation Graph
  * take a series of serialization events
  * discard serialization details
  * produces a representation graph  
3. Constructing Native Data Structures
  * base on only information available in the representation

### 3.2 Information Models
Separating properties needed for serialization and presentation for cross-languange consistency and portability of representations of application information.  

**Figure 3.2 Information Models**  
![Information Models](./images/information-models.png)  
where  
* Full arrows denote composition
* hollow arrows denote inheritance,
* “1” and “*” denote “one” and “many” relationships
* A single “+” denotes serialization details
* A double “++” denotes presentation details

#### 3.2.1 Representation Graph
Representation of native data structure is a rooted, connected, directed graph of tagged nodes.

*Note*:
  * All the nodes must be reachable from the root node via such edges
  * The YAML graph may include cycles
  * A node may have more than one incoming edge

**Nodes**
  * collections: nodes that are defined in terms of other nodes
  * scalars: nodes that are independent of any other nodes

**Figure 3.3 Representation Model**  
![Representation Model](./images/representation-model.png)

##### 3.2.1.1 Nodes
* Represents a single native data structure
* Has a tag serving to restrict the set of possible content values
* 3 kinds: 
  + *scalar*: An opaque datum that can be presented as a series of zero or more Unicode characters.
  + *sequence*: an ordered series of zero or more nodes
  + *mapping*: an unordered set of `key: value` node pairs, with `key` being unique

*Note*: Considering sequences and mappings together as *collections*, where sequences are treated as mappings with integer keys starting at zero

##### 3.2.1.2 Tags
**Definition**: Identifier representing type information of native data structures  

2 types:
  * *Global tags*: URIs with recommended `tag:` URI scheme, globally unique across all applications
  * *Local tags*: not URIs, starting with `!`, not expected to be globally unique

**Relationship**
  * No mandatory special relationship between different tags with the same prefix
  * Conventions  
    + fragments(containing `#`) are used to identify different "variants" of a tag
    + `/` is used to define nested tag "namespace" hierarchies

**Functions**
  * Associate meta information with each node
  * Must specify the expected node kind (scalar/sequence/ mapping
  * Scalar tags must also provide a mechanism for converting formatted content to a canonical form for supporting equality testing
  * May provide additional information for validation, tag resolution

##### 3.2.1.3 Node Comparison
A mechanism for testing the equality of nodes to tackle the examing of uniqueness of keys in mappings.

* **Canonical Form**: Tesing scalar equality by requiring that every scalar tag must specify a mechanism for producing the *canonical form* of any formatted content
* **Equality**: The same tag(=kind) and equal content
* **Identity**: Two nodes are *identical* only when they represent the same native data structure(the same memory address)

#### 3.2.2 Serialization Tree
Serialization tree are produced by imposing an order on mapping keys and employ alias nodes to indicate a subsequent occurrence of a previously encountered node.

**Figure 3.4. Serialization Model**  
![Serialization Model](./images/serialization-model.png)

##### 3.2.2.1 Keys Order
Unlike representation model, it is necessary to impose an *ordering* (=a serialization detail) on its keys as as to serialize a mapping.

##### 3.2.2.2 Anchors and Aliases
During serialization, for nodes appearing in more than one collections
* The first occurrence is identified by an *anchor*
* Each subsequent occurrence is serialized as an *alias* node refering to *anchor*

*Note*: Anchor names are a serialization detail. When composing a representation graph from serialized events, an alias node refers to the most recent node in the serialization having the specified anchor.

#### 3.2.3 Presentation Stream
**Presentation**: A stream of Unicode characters making use of of styles, scalar content formats, comments, directives and other presentation details to present a YAML serialization in a human readable way

**Figure 3.5 Presentation Model**  
![Presentation Model](./images/presentation-model.png)

##### 3.2.3.1 Node Styles 
Each node is presented in some *style* based on its kind.

**2 styles**
  * block styles: structure by indentation
  * flow styles: structure by explicit indicators

**Scalar styles**
  * Block scalar styles 
    + the literal style
    + the folded style
  * Flow scalar styles
    + the plain style
    + the single-quoted style
    + the double-quoted style

Normally, block sequences and mappings begin on the next line.

**Figure 3.6 Kind/Style Combinations**  
![Kind/Style Combinations](./images/kind-style-combinations.png)

##### 3.2.3.2 Scalar Formats 
Eg. decimal/oct/hexadecimal

##### 3.2.3.3 Comments 
* Not associated with a particular node
* Serve for communication for maintainance
* Typically used in configuration file

##### 3.2.3.4 Directives 
**Directive**=name+additional optionals

Directives are instructions to the YAML processor.

Currently, only two **"YAML"** and **"TAG"**

### 3.3 Loading Failure Points
The process of loading native data structures from a YAML stream has several potential *failure points*.

**Figure 3.4 Loading Failure Points**  
![Loading Failure Points](./images/loading-failure-points.png)

**2 types of representation**
  * partial representation
    + no resolution for the tag of each node
    + no availablity of the canonical form of formatted scalar content
  * complete representation
    + specification of the tag of each node
    + the canonical form of formatted scalar content

*Note*: A complete representation is required in order to construct native data structures.

#### 3.3.1 Well-Formed Streams and Identified Aliases
A well-formed character stream must match the BNF productions.  
A YAML processor 
  * **should** reject ill-formed streams and unidentified aliases
  * **may** recover from syntax errors, possibly by ignoring certain parts of the input, but **must** provide a mechanism for reporting such errors.

#### 3.3.2 Resolved Tags
Typically, most tags are not explicitly specified in the character stream. During parsing, if lacking an explicit tag , non-plain scalars are given a non-specific tag: `!` and `?` for all other nodes.  

**3 only parameters** for resolving tags
  * the non-specific tag of the node
  * the path leading from the root to the node
  * the content (and hence the kind) of the node

*Note*: Tag resolution for *alias* must depend only on the path to the first *anchor*

**Tag resolution** **must** consider 
  * presentation details (eg. comments)
  * the content of any other node (except for the content of the key nodes directly along the path leading from the root to the resolved node)
  * the content of a sibling node in a collection
  * the content of the value node associated with a key node being resolved

*tag resolution convention* for **non-specific tag `!`**
  * resolve to "tag:yaml.org,2002:seq"/"tag:yaml.org,2002:map"/"tag:yaml.org,2002:str" based on node kinds 

**Application specific tag resolution rules** should be restricted to resolving the `?` non-specific tag (mostly for plain scalars)

*Note*: Tag resolution is specific to the application and can be overridden and expanded by applications.

#### 3.3.3 Recognized and Valid Tags
  A valid node has a tag recognizable by the YAML processor and its cotent satisfying the constraints imposed by this tag.  
  For **scalar node** with an unrecognized tag or invalid content, only a partial representation may be composed. In contrast, a YAML processor can always compose a complete representation for an unrecognized or an invalid collection, since collection equality does not depend upon knowledge of the collection's data type, but such representation is usable for constructing a native data structure.

#### 3.3.4 Available Tags
  No native data structure for a node with unvailable tags.