# Install the Latest Docker on Ubuntu 16.04

# Outline
1. [Set up the Docker repository](#1-set-up-the-docker-repository)  
	1.1 [Install packages to allow `apt` to use a repository over https](#1.1-install-packages-to-allow-apt-to-use-a-repository-over-https)  
	1.2 [Add Docker's official GPG key](#12-add-dockers-official-gpg-key)  
	1.3 [Update the reporitory file](#13-update-the-reporitory-file)  
2. [Install Docker](#2-install-docker)  
	2.1 [Update the apt package index](#21-update-the-apt-package-index)  
	2.2 [2 Strageties of installtion](#22-2-strageties-of-installtion)  
		2.2.1 [Install the latest version of Docker](#221-install-the-latest-version-of-docker)  
		2.2.2 [Install a specific version of Docker on production systems](#222-install-a-specific-version-of-docker-on-production-systems)  
	2.3 [Verify the installation](#23-verify-the-installation)  
3. [Uninstall Docker](#3-uninstall-docker)  
	3.1 [Uninstall the Docker package](#31-uninstall-the-docker-package)  
	3.2 [Delete all images, containers and volume if any](#32-delete-all-images-containers-and-volume-if-any)  
4. [Post-installation](#4-post-installation)  
	4.1 [Create a Unix group called `docker` if it doesn't exists yet](#41-create-a-unix-group-called-docker-if-it-doesnt-exists-yet)  
	4.2 [Add your users to group `docker`](#42-add-your-users-to-group-docker)  
	4.3 [Log out and log back in](#43-log-out-and-log-back-in)  
	4.4 [Verify that you can `docker` commands without `sudo`](#44-verify-that-you-can-docker-commands-without-sudo)  

## Prerequisites
Docker requires a 64-bit installation regardless of Ubuntu version.

## 1. Set up the Docker repository
### 1.1 Install packages to allow `apt` to use a repository over https   
```bash
sudo apt install apt-transport-https ca-certificates
```

### 1.2 Add Docker's official GPG key  
```bash
curl -s http://yum.dockerproject.org/gpg | sudo apt-key add
```
> **Note**: The URL is correct, even for Linux distributions that use APT.

Verify that the key ID is `58118E89F3A912897C070ADBF76221572C52609D`.  
```bash
apt-key fingerprint 58118E89F3A912897C070ADBF76221572C52609D
pub   4096R/2C52609D 2015-07-14
      Key fingerprint = 5811 8E89 F3A9 1289 7C07  0ADB F762 2157 2C52 609D
uid                  Docker Release Tool (releasedocker) <docker@docker.com>
```

### 1.3 Update the reporitory file  
Open the /etc/apt/sources.list.d/docker.list file in your favorite editor.  If the file doesn't exist, create it.  
Clear all content of this **docker.list** file, and add following entry
```
deb https://apt.dockerproject.org/repo ubuntu-xenial main
```
Save and close `/etc/apt/sources.list.d/docker.list`

## 2. Install Docker
### 2.1 Update the apt package index  
```
sudo apt update
```

### 2.2 2 Strageties of installtion
#### 2.2.1 Install the latest version of Docker  
```
sudo apt -y install docker-engine
```

> **Warning**: If you have both stable and unstable repositories enabled,
> installing or updating without specifying a version in the apt-get
> install or apt-get update command will always install the highest
> possible version, which will almost certainly be an unstable one.

#### 2.2.2 Install a specific version of Docker on production systems  
```
docker-engine | 1.13.0-0~ubuntu-xenial | https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
docker-engine | 1.12.6-0~ubuntu-xenial | https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
docker-engine | 1.12.5-0~ubuntu-xenial | https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
docker-engine | 1.12.4-0~ubuntu-xenial | https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
docker-engine | 1.12.3-0~xenial | https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
docker-engine | 1.12.2-0~xenial | https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
docker-engine | 1.12.1-0~xenial | https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
docker-engine | 1.12.0-0~xenial | https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
docker-engine | 1.11.2-0~xenial | https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
docker-engine | 1.11.1-0~xenial | https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
docker-engine | 1.11.0-0~xenial | https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
```
where The second column is the version string. The third column is the repository name, which indicates which repository the package is from and by extension its stability level. To install a specific version, append the version string to the package name and separate them by an equals sign (=):
```
sudo apt -y install docker-engine=<VERSION_STRING>
```
The Docker daemon starts automatically.  
### 2.3 Verify the installation  
Test by running the hello-world image  
```
sudo docker run hello-world
```
This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits. If no error is reported, then the installation is ok.

> **Note**: Docker is installed and running. You need to use `sudo` to run 
> Docker commands. Continue to **postinstall** part below to allow non-privileged 
> users to run Docker commands and for other optional configuration steps.

## 3. Uninstall Docker
### 3.1 Uninstall the Docker package  
```
sudo apt purge docker-engine
```
### 3.2 Delete all images, containers, and volume if any  
```
sudo rm -rf /var/lib/docker
```

## 4. Post-installation
The `docker` daemon binds to a Unix socket instead of a TCP port. By default that Unix socket is owned by the user `root` and other users can access it with `sudo`. For this reason, `docker` daemon always runs as the `root` user.  
Since when the `docker` daemon starts, it makes the ownership of the Unix socket read/writable by the `docker` group, to avoid having to use `sudo` when you use the `docker` command, do as follows  
### 4.1 Create a Unix group called `docker` if it doesn't exists yet  
```
sudo groupadd docker
```
### 4.2 Add your users to group `docker`
```
sudo usermod -aG docker $USER
```
### 4.3 Log out and log back in  
This ensures that your group membership is re-evaluated.  
### 4.4 Verify that you can `docker` commands without `sudo`  
```
docker run hello-world
```

