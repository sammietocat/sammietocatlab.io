# Upgrade to the Latest Kernel for CentOS 7 [2017-01-19]

## Outline
1. [Import the necessary key for **ELRepo** repository](#1-import-the-necessary-key-for-elrepo-repository)
2. [Install **ELRepo** repository for CentOS-7](#2-install-elrepo-repository-for-centos-7)
3. [Install the kernel from the *mainline*, which is the lastest kernel in **ELRepo** repository](#3-install the kernel from the *mainline*, which is the lastest kernel in **elrepo** repository)
4. [Revise boot order of kernel. Default order is 1. New kernel are inserted in the beginning, i.e., 0](#4-revise-boot-order-of-kernel.-default-order-is-1.-new-kernel-are-inserted-in-the-beginning,-i.e.,-0)
5. [Reboot to take effect](#5-reboot-to-take-effect)
6. [Check kernel version](#6-check-kernel-version)

## 1. Import the necessary key for **ELRepo** repository  
```bash
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
```

## 2. Install **ELRepo** repository for CentOS-7
```bash
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
```

## 3. Install the kernel from the *mainline*, which is the lastest kernel in **ELRepo** repository  
```bash
yum --enablerepo=elrepo-kernel install  kernel-ml-devel kernel-ml -y
```

## 4. Revise boot order of kernel. Default order is 1. New kernel are inserted in the beginning, i.e., 0   
```bash
grub2-set-default 0
```

## 5. Reboot to take effect  
```bash
reboot
```

## 6. Check kernel version  
```bash
uname -r
```
