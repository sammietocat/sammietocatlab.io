# [Static, Shared Dynamic and Loadable Linux Libraries](http://www.yolinux.com/TUTORIALS/LibraryArchives-StaticAndDynamic.html) [2016-12-17]
This is a note about some basic knowledge of static libraries and shared dynamic libraries used in linux

## Outline
1. [Why libraries are used](#1)
2. [Linux Library Types](#2)
3. [Static Libraries: (.a)](#3)
4. [Dynamically Linked "Shared Object" Libraries: (.so)](#4)
    * [Library Path](#4-1)

<h2 id="1">Why libraries are used</h2>
+ Definition of Libraries: A single file grouping  together multiple compiled object code files
+ Eg. C standard libraries and C++ STL
+ Benefit: Reference the individual library rather than stating each and every object file when linking
    - Simplify the multiple use and sharing of software components between applications
    - Allow application vendors a way to simply release an API to interface with an application
    - The dynamic library remain separate from the executable reducing it's size and thus disk space used

<h2 id="2">Linux Library Types</h2>

### Type  
1. **Static libraries (.a)**: Library of object code which is linked with, and becomes part of the application.
2. **Dynamically linked shared object libraries (.so)**: used in 2 ways
    + Dynamically linked at run time but statically aware. The libraries must be available during compile/link phase. The shared objects are not included into the executable component but are tied to the execution.
    + Dynamically loaded/unloaded and linked during execution (i.e. browser plug-in) using the dynamic linking loader system functions

### Naming conventions  
+ Typically names with the prefix **"lib"**
+ When linking, the command line reference to the library **without** library prefix or suffix.
+ Eg.  
```bash
g++ src-file.cpp -lm -lpthread
```
The linking libraries referenced are the `math` library and the `thread` library. They are found in `/usr/lib/libm.a` and `/usr/lib/libpthread.a`.

<h2 id="3">Static Libraries: (.a)</h2>
### Example files  
+ `test1.cpp`  
```c++
void test1(int *i)
{
    *i=5;
}
```
+ `test2.cpp`  
```c++
void test2(int *i)
{
    *i=100;
}
```
+ `prog.cpp`  
```c++
#include <iostream>

using namespace std;

void test1(int *i);
void test2(int *i);

int main()
{
	int x;

	test1(&x);
	cout << "Valx=" << x << endl;

	return 0;
}
```

We are going to run the demo with a directory structure like  
```
|--src  
|   |--prog.cpp
|   |--test1.cpp
|   |--test2.cpp
|--lib
```
where `lib` directory is to store the generate library files.

### Steps to generate static library
1. **Compile**   
    ```bash
    g++ -Wall -c src/test1.cpp -o src/test1.o
    g++ -Wall -c src/test2.cpp -o src/test2.o
    ``` 
    where option `-Wall` denotes including warnings  
2. **Create** library **"libtest.a"**  
    ```bash
    ar -cvq libtest.a ./src/test1.o ./src/test2.o
    mkdir lib && mv libtest.a ./lib
    ```
3. List files in library  
    ```bash
    ar -t ./lib/libtest.a
    ```
4. **Linking** with the library  
    ```bash
    g++ -o prog src/prog.cpp -L./lib -ltest
    ```
    Compiler options
        + `-L`: specify the directory containing the `.a` files
        + `-ltest`: link library `libtest.so` as stated before  
    If it is placed in current directory, we can also link the library as
    ```bash
    g++ -o prog src/prog.cpp libtest.a
    ```

**Note**: the corresponding files are in the `static-libraries-demo.tar.xz`

<h2 id="4">Dynamically Linked "Shared Object" Libraries: (.so)</h2>
### Example files  
same to those in static library generation part

### Steps to generate "Shared Object" Libraries  
1. **Create** object code  
    ```bash
    g++ -Wall -fPIC -c src/test1.cpp -o src/test1.o
    g++ -Wall -fPIC -c src/test2.cpp -o src/test2.o
    ```
2. **Create** library  
    ```bash
    g++ -shared -Wl,-soname,libtest.so.1 -o libtest.so.1.0 ./src/test1.o ./src/test2.o
    ```
3. Optional: create default version using a symbolic link  
    ```bash
    mkdir lib && mv libtest.so.1.0 ./lib
    cd ./lib && ln -sf libtest.so.1.0 libtest.so.1
    cd ./lib && ln -sf libtest.so.1.0 libtest.so
    ```
    or
    ```bash
    mkdir lib && mv libtest.so.1.0 ./lib
    cd ./lib && ln -sf libtest.so.1.0 libtest.so.1
    cd ./lib && ln -sf libtest.so.1 libtest.so
    ```

    Compiler options  
    + `-Wall`: include warnings
    + `-fPIC`: Compiler directive to output position independent code, a characteristic required by shared libraries. Also see "-fpic".
    + `-shared`: Produce a shared object which can then be linked with other objects to form an executable.
    + `-Wl,options`: Pass `options` to linker. 
    In this example the options to be passed on to the linker are: "-soname libctest.so.1". The name passed with the "-o" option is passed to gcc.
    + `-o`: Output of operation.

    Library Links:
    + The link to `./lib/libctest.so` allows the naming convention for the compile flag -lctest to work.
    + The link to `./lib/libctest.so.1` allows the run time binding to work.

### Compile main program and link with shared object library
Compiling for runtime linking with a dynamically linked `libctest.so.1.0`:  
```bash
g++ -L./lib src/prog.cpp -ltest -o prog
```
Where the name of the library is `libctest.so`. (This is why you must create the symbolic links or you will get the error `"/usr/bin/ld: cannot find -lctest"`.)   
The libraries will **NOT** be included in the executable but will be dynamically linked during runtime execution.

### List Dependencies  
+ Command: `ldd name-of-executable`
+ Eg. for `prog` above  
    ```bash
    ldd prog 
        linux-vdso.so.1 =>  (0x00007ffd9cd3e000)
        libtest.so.1 => not found
        libstdc++.so.6 => /lib64/libstdc++.so.6 (0x00007fc5af1e4000)
        libm.so.6 => /lib64/libm.so.6 (0x00007fc5aeee2000)
        libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007fc5aeccc000)
        libc.so.6 => /lib64/libc.so.6 (0x00007fc5ae909000)
        /lib64/ld-linux-x86-64.so.2 (0x00007fc5af503000)
    ```
    As we can see, `libtest.so.1` is problematic since `./libtest.so.1` is not in the default system library directories, then we need to resolve this dependencies by one of following ways  
    
    - Add the unresolved library path in `/etc/ld.so.conf.d/name-of-lib-x86_64.conf` and/or `/etc/ld.so.conf.d/name-of-lib-i686.conf`   
        Reload the library cache (`/etc/ld.so.cache`) with the command:   
        ```
        sudo ldconfig
        ```  

    - Add library and path explicitly to the compiler/linker command:  
        ```
        -lname-of-lib -L/path/to/lib
        ```

    - Add the library path to the environment variable  
        ```bash
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/lib
        ```

### Run `prog`
We use the 3rd ways above to specify the path of `libtest.so.1`, then run the program as  
```bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./lib
./prog
```

<h3 id="4-1">Library Path</h3>
In order for an executable to find the required libraries to link with during run time, one **must** configure the system so that the libraries can be found. Methods available: (Do at least one of the following)  
1. Add library directories to be included during dynamic linking to the file `/etc/ld.so.conf`  
Sample: `/etc/ld.so.conf`  
```
/usr/X11R6/lib
/usr/lib
...
..
/usr/lib/sane
/usr/lib/mysql
/opt/lib
```
Add the library path to this file and then execute the command (as root) ldconfig to configure the linker run-time bindings.  
You can use the "-f file-name" flag to reference another configuration file if you are developing for different environments. 
2. Add specified directory to library cache: (**as root**)   
```bash
ldconfig -n /opt/lib 
```
Where `/opt/lib` is the directory containing your library `libctest.so`.  
When developing and just adding your current library directory: 
```bash
cd ./lib && ldconfig -n
```
then back to parent directory, run `prog` with `./prog -L./lib -ltest`.  
This will **NOT** permanently configure the system to include this directory. The information will be lost upon system reboot.  
+ Specify the environment variable LD_LIBRARY_PATH to point to the directory paths containing the shared object library. This will specify to the run time loader that the library paths will be used during execution to resolve dependencies.   
    - Linux/Solaris: `LD_LIBRARY_PATH`
    - SGI: `LD_LIBRARYN32_PATH`
    - AIX: `LIBPATH`
    - Mac OS X: `DYLD_LIBRARY_PATH`
    - HP-UX: `SHLIB_PATH`

Example (bash shell): 
```bash
export LD_LIBRARY_PATH=/path/to/lib-dir:$LD_LIBRARY_PATH
``` 
or add to your `~/.bashrc file`:
```
...
if [ -d /opt/to/lib-dir ];
then
   LD_LIBRARY_PATH=/opt/lib-dir:$LD_LIBRARY_PATH
fi

...

export LD_LIBRARY_PATH
```
This instructs the run time loader to look in the path described by the environment variable `LD_LIBRARY_PATH`, to resolve shared libraries. This will include the path `/opt/to/lib-dir`.

**Note**: the corresponding files are in the `shared-libraries-demo.tar.xz`
