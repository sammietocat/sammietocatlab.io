# 15 Useful "ifconfig" Commands to Configure Network Interface [2017-01-09]

## Outline
* [Overview](#overview)
* [1. View All Network Setting](#1-view-all-network-setting)
* [2. Display Information of All Network Interfaces](#2-display-information-of-all-network-interfaces)
* [3. View Network Settings of Specific Interface](#3-view-network-settings-of-specific-interface)
* [4. Enable an Network Interface](#4-enable-an-network-interface)
* [5. Disable an Network Interface](#5-disable-an-network-interface)
* [6. Assign a IP Address to Network Interface](#6-assign-a-ip-address-to-network-interface)
* [7. Assign a Netmask to Network Interface](#7-assign-a-netmask-to-network-interface)
* [8. Assign a Broadcast to Network Interface](#8-assign-a-broadcast-to-network-interface)
* [9. Assign a IP, Netmask and Broadcast to Network Interface](#9-assign-a-ip-netmask-and-broadcast-to-network-interface)
* [10. Change MTU for an Network Interface](#10-change-mtu-for-an-network-interface)
* [11. Enable Promiscuous Mode](#11-enable-promiscuous-mode)
* [12. Disable Promiscuous Mode](#12-disable-promiscuous-mode)
* [13. How to Add New Alias to Network Interface](#13-how-to-add-new-alias-to-network-interface)
* [14. Remove Alias to Network Interface](#14-remove-alias-to-network-interface)
* [15. Change the MAC address of Network Interface](#15-change-the-mac-address-of-network-interface)

### Overview
`ifconfig`, short for, **interface configuration**, is an utility for system/network administration in Unix/Linux to configure, manage and query network interface parameters via command line interface or in a system configuration scripts.

The `ifconfig` command is used for
* diplay current network configuration information
* set up an ip address, netmask
* broadcast address to an network interface
* create an alias for network
* set up hardware address
* enable/disable network interfaces

*Note*: `ifconfig` is deprecated and replaced by `ip` command in most Linux distributions
*Remark*: All demonstration are carried in Centos 7.2 1511

### 1. View All Network Setting
`ifconfig` with no arguments will display all the **active** interface details.  
```bash
[hello@world ~]$ ifconfig
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.109  netmask 255.255.255.0  broadcast 192.168.0.255
        inet6 fe80::2e44:fdff:fe13:14b4  prefixlen 64  scopeid 0x20<link>
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 68067  bytes 49740642 (47.4 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 49561  bytes 8770757 (8.3 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 0  (Local Loopback)
        RX packets 493  bytes 45567 (44.4 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 493  bytes 45567 (44.4 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:c4:13:09  txqueuelen 0  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

### 2. Display Information of All Network Interfaces
`ifconfig` command with `-a` argument will display information of all **active** or **inactive** network interfaces on server.  
```bash
[hello@world ~]$ ifconfig -a
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.109  netmask 255.255.255.0  broadcast 192.168.0.255
        inet6 fe80::2e44:fdff:fe13:14b4  prefixlen 64  scopeid 0x20<link>
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 72903  bytes 53721277 (51.2 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 53508  bytes 9393414 (8.9 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 0  (Local Loopback)
        RX packets 493  bytes 45567 (44.4 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 493  bytes 45567 (44.4 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:c4:13:09  txqueuelen 0  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0-nic: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 52:54:00:c4:13:09  txqueuelen 500  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

### 3. View Network Settings of Specific Interface
`ifconfig` with interface name as argument will display details of specific network interface with that name.  
```
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.109  netmask 255.255.255.0  broadcast 192.168.0.255
        inet6 fe80::2e44:fdff:fe13:14b4  prefixlen 64  scopeid 0x20<link>
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 73142  bytes 53789134 (51.2 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 53630  bytes 9420949 (8.9 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  
```

### 4. Enable an Network Interface
`ifup` flag with interface name activates an network interface, if it is not in active state. Eg, `ifup eno1` will activate the **eno1** interface.  
```bash
[hello@world ~]$ sudo ifup eno1
```

### 5. Disable an Network Interface
`ifdown` flag with interface name deactivates an network interface, if it is not in active state. Eg, `ifdown eno1` will deactivate the **eno1** interface.  
```bash
[hello@world ~]$ sudo ifdown eno1 up
```

### 6. Assign a IP Address to Network Interface
`ifconfig` with interface name and ip as arguments assign a ip address to a specific network interface. Eg, `ifconfig eno1 192.168.0.110` will set the ip of interface **eno1** to **192.168.0.110**
```bash
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.109  netmask 255.255.255.0  broadcast 192.168.0.255
        inet6 fe80::2e44:fdff:fe13:14b4  prefixlen 64  scopeid 0x20<link>
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 75099  bytes 54346428 (51.8 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54384  bytes 9576405 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

[hello@world ~]$ sudo ifconfig eno1 192.168.0.110

[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.110  netmask 255.255.255.0  broadcast 192.168.0.255
        inet6 fe80::2e44:fdff:fe13:14b4  prefixlen 64  scopeid 0x20<link>
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 75136  bytes 54354892 (51.8 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54400  bytes 9579205 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000 
```

### 7. Assign a Netmask to Network Interface
`ifconfig` with interface name and `netmask` flag and netmask value as arguments defines an netmask to an given interface. `ifconfig eno1 netmask 255.255.255.224` will set the network mask to an given interface **eno1**.  
```bash
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.110  netmask 255.255.255.0  broadcast 192.168.0.255
        inet6 fe80::2e44:fdff:fe13:14b4  prefixlen 64  scopeid 0x20<link>
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 75431  bytes 54427894 (51.9 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54452  bytes 9583978 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

[hello@world ~]$ sudo ifconfig eno1 netmask 255.255.255.224
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.110  netmask 255.255.255.224  broadcast 192.168.0.127
        inet6 fe80::2e44:fdff:fe13:14b4  prefixlen 64  scopeid 0x20<link>
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 75458  bytes 54435527 (51.9 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54460  bytes 9585788 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  
```

### 8. Assign a Broadcast to Network Interface
`ifconfig` with interface name and `broadcast` flag and netmask value as arguments will set the broadcast address for the given interface. For example, `ifconfig eno1 broadcast 192.168.0.254` command sets the broadcast address of an interface **eno1** to `192.168.0.254`.  
```bash
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.110  netmask 255.255.255.224  broadcast 192.168.0.255
        inet6 fe80::2e44:fdff:fe13:14b4  prefixlen 64  scopeid 0x20<link>
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 75665  bytes 54492462 (51.9 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54476  bytes 9589870 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

[hello@world ~]$ sudo ifconfig eno1 broadcast 192.168.0.254
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.110  netmask 255.255.255.224  broadcast 192.168.0.254
        inet6 fe80::2e44:fdff:fe13:14b4  prefixlen 64  scopeid 0x20<link>
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 75678  bytes 54495927 (51.9 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54484  bytes 9592007 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  
```

### 9. Assign a IP, Netmask and Broadcast to Network Interface
To assign an IP address, Netmask address and Broadcast address all at once using `ifconfig` command with all arguments as given below.  
```bash
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.110  netmask 255.255.255.224  broadcast 192.168.0.255
        inet6 fe80::2e44:fdff:fe13:14b4  prefixlen 64  scopeid 0x20<link>
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 75837  bytes 54537397 (52.0 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54487  bytes 9592649 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

[hello@world ~]$ sudo ifconfig eno1 192.168.0.111 netmask 255.255.255.0 broadcast 192.168.0.253
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.111  netmask 255.255.255.0  broadcast 192.168.0.253
        inet6 fe80::2e44:fdff:fe13:14b4  prefixlen 64  scopeid 0x20<link>
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 76208  bytes 54630769 (52.0 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54520  bytes 9599031 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  
```

### 10. Change MTU for an Network Interface
The `mtu` argument set the maximum transmission unit to an interface. The **MTU** allows you to set the limit size of packets that are transmitted on an interface. The **MTU** able to handle maximum number of octets to an interface in one single transaction. Eg, `ifconfig eno1 mtu 1000` will set the maximum transmission unit to given set (i.e. 1000). Not all network interfaces supports **MTU** settings.  
```bash
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.111  netmask 255.255.255.0  broadcast 192.168.0.253
        inet6 fe80::2e44:fdff:fe13:14b4  prefixlen 64  scopeid 0x20<link>
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 76571  bytes 54700364 (52.1 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54584  bytes 9606288 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

[hello@world ~]$ sudo ifconfig eno1 mtu 1000
[hello@world ~]$ ifconfig eno1
eno1: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1000
        inet 192.168.0.111  netmask 255.255.255.0  broadcast 192.168.0.253
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 76632  bytes 54711948 (52.1 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54602  bytes 9608362 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  
```

### 11. Enable Promiscuous Mode
What happens in normal mode, when a packet received by a network card, it verifies that the packet belongs to itself. If not, it drops the packet normally, but in the promiscuous mode is used to accept all the packets that flows through the network card.  
Most of the today’s network tools uses the promiscuous mode to capture and analyze the packets that flows through the network interface. To set the promiscuous mode for interface `eno1`, use `sudo ifconfig eno1 promisc` as follows,  
```bash
[hello@world ~]$ ifconfig eno1
eno1: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1000
        inet 192.168.0.111  netmask 255.255.255.0  broadcast 192.168.0.253
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 76632  bytes 54711948 (52.1 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54602  bytes 9608362 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

[hello@world ~]$ sudo ifconfig eno1 promisc
[hello@world ~]$ ifconfig eno1
eno1: flags=4419<UP,BROADCAST,RUNNING,PROMISC,MULTICAST>  mtu 1280
        inet 192.168.0.111  netmask 255.255.255.0  broadcast 192.168.0.253
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 76861  bytes 54763325 (52.2 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54779  bytes 9646843 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000 
```

### 12. Disable Promiscuous Mode
`ifconfig` with `-promisc` as flag will switch off promisc mode.  
```bash
[hello@world ~]$ ifconfig eno1
eno1: flags=4419<UP,BROADCAST,RUNNING,PROMISC,MULTICAST>  mtu 1280
        inet 192.168.0.111  netmask 255.255.255.0  broadcast 192.168.0.253
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 76861  bytes 54763325 (52.2 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54779  bytes 9646843 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

[hello@world ~]$ sudo ifconfig eno1 -promisc
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1280
        inet 192.168.0.111  netmask 255.255.255.0  broadcast 192.168.0.253
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 76971  bytes 54784302 (52.2 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54808  bytes 9650824 (9.2 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  
```

### 13. How to Add New Alias to Network Interface
The `ifconfig` utility allows you to configure additional network interfaces using alias feature. To add alias `eno1:0` network interface of `eno1`, use `sudo ifconfig eno1:0 192.168.0.112`. Please note that alias network address in **same sub-net mask**. Eg, if your `eno1` network ip address is `192.168.0.111`, then alias ip address should be `192.168.0.112` in the same sub-net mask.  
```bash
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1280
        inet 192.168.0.111  netmask 255.255.255.0  broadcast 192.168.0.253
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 76971  bytes 54784302 (52.2 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 54808  bytes 9650824 (9.2 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

[hello@world ~]$ sudo ifconfig eno1:0 192.168.0.112
[hello@world ~]$ ifconfig eno1:0
eno1:0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1280
        inet 192.168.0.112  netmask 255.255.255.0  broadcast 192.168.0.255
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        device interrupt 20  memory 0xf7c00000-f7c20000  
```

### 14. Remove Alias to Network Interface
Remove a given alias `eno1:0` network by `sudo ifconfig eno1:0 down`  
```bash
[hello@world ~]$ ifconfig
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1280
        inet 192.168.0.111  netmask 255.255.255.0  broadcast 192.168.0.253
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 79022  bytes 55205854 (52.6 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 55867  bytes 9827851 (9.3 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

eno1:0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1280
        inet 192.168.0.112  netmask 255.255.255.0  broadcast 192.168.0.255
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        device interrupt 20  memory 0xf7c00000-f7c20000  

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 0  (Local Loopback)
        RX packets 529  bytes 48167 (47.0 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 529  bytes 48167 (47.0 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:c4:13:09  txqueuelen 0  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

[hello@world ~]$ sudo ifconfig eno1:0 down
[hello@world ~]$ ifconfig
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1280
        inet 192.168.0.111  netmask 255.255.255.0  broadcast 192.168.0.253
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 79032  bytes 55208536 (52.6 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 55877  bytes 9828672 (9.3 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 0  (Local Loopback)
        RX packets 529  bytes 48167 (47.0 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 529  bytes 48167 (47.0 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:c4:13:09  txqueuelen 0  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

### 15. Change the MAC address of Network Interface
To change the **MAC (Media Access Control)** address of a given network interface, use `ifconfig` command with argument `hw ether`. Eg, `sudo ifconfig eno1 hw ether AA:BB:CC:DD:EE:FF` will set the **MAC** address of `eno1` to `AA:BB:CC:DD:EE:FF`  
```bash
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1280
        inet 192.168.0.111  netmask 255.255.255.0  broadcast 192.168.0.253
        ether 2c:44:fd:13:14:b4  txqueuelen 1000  (Ethernet)
        RX packets 79877  bytes 55435528 (52.8 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 56425  bytes 9928684 (9.4 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

[hello@world ~]$ sudo ifconfig eno1 hw ether AA:BB:CC:DD:EE:FF
[hello@world ~]$ ifconfig eno1
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1280
        inet 192.168.0.111  netmask 255.255.255.0  broadcast 192.168.0.253
        ether aa:bb:cc:dd:ee:ff  txqueuelen 1000  (Ethernet)
        RX packets 79941  bytes 55454635 (52.8 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 56454  bytes 9933431 (9.4 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device interrupt 20  memory 0xf7c00000-f7c20000  

```
