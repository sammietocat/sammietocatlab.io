# Sammietocat's Gitlab Page

## Overview  
This project serves as a Wiki page for sammietocat. All the projects developed by sammietocat will be published in this Pages project.  

## Change Log  
## [2017-07-23]  
+ **Updated**  
	- The Golang logo in `Home` page  
	- `404` page  

## [2017-07-23]  
+ **Added**  
	- a side navigation panel  
+ **Updated**  
	- refactor the structure of the project  
	- rename the `notebook` part as `moments`  

### [2017-07-09]  
+ **Added**  
	- move the project to gitlab  
	- `.gitlab-ci.yml` file for CI build for the wiki pages    
	- a `404` page   
+ **Updated**  
	- the header and footer of each page  

### [v-1.3] - 2017-01-20
#### Added  
+ Notes of "Install the Latest Docker on Ubuntu 16.04" in **Notebook/Linux** panel

### [v-1.2] - 2017-01-19
#### Added  
+ Add notes of "Upgrade to the Lastest Kernel for CentOS 7" in **Notebook/Linux** topic    

#### Fixed  
+ Recategorize "Static-Shared-Dynamic-and-Loadable-Linux-Libraries" from **Notebook/C++** to **Notebook/Linux** topic

### [v-1.0] - 2016-12-17
#### Added
+ The **Notebook** tab
+ Add notes of "Static-Shared-Dynamic-and-Loadable-Linux-Libraries" in `C++` topic

### [v-0.3] - 2016-11-07
#### Added
+ A fixed action button at the botton right of the page [no 'actual' yet]  

#### Revised  
+ Make the `navbar-wrapper` a container instead of leaving it default to full screen
